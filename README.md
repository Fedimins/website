# Fedimins-Website

[![Codeberg CI](https://ci.codeberg.org/api/badges/Fedimins/website/status.svg)](https://ci.codeberg.org/Fedimins/website)
![Framework](https://img.shields.io/badge/Framework-hugo-pink)
![Lizenz](https://img.shields.io/badge/Lizenz-CC--BY--NC--SA--4.0-orange)

Dieses Repository enthält den Quellcode für die Fedimins-Website. Die Fedimins-Website richtet sich an Inhaber, Administratoren und Moderatoren und stellt Informationen und Ressourcen rund um das Fediverse und die verschiedenen Dienste und Plattformen bereit, die Teil des Fediverse sind.

## Inhaltsverzeichnis

- [Fedimins-Website](#fedimins-website)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Über die Fedimins-Website](#über-die-fedimins-website)
  - [Installation](#installation)
  - [Verwendung](#verwendung)
  - [Beitragende](#beitragende)
  - [Kontakt](#kontakt)
  - [Lizenz](#lizenz)

## Über die Fedimins-Website

Die Fedimins-Website wurde entwickelt, um Inhabern, Administratoren und Moderatoren im Fediverse Informationen zur Verfügung zu stellen. Das Fediverse ist ein dezentrales Netzwerk von sozialen Plattformen, die auf offenen Standards basieren. Es bietet eine Alternative zu zentralisierten sozialen Netzwerken und ermöglicht es Benutzern, ihre Daten und ihre Privatsphäre besser zu kontrollieren.

Die Fedimins-Website bietet Erklärungen und Anleitungen zu verschiedenen Diensten und Plattformen im Fediverse, darunter soziale Netzwerke, Mikroblogging-Plattformen, Bild- und Video-Hosting-Dienste und vieles mehr. Sie enthält Ressourcen und Informationen, die Inhabern, Administratoren und Moderatoren dabei helfen, ihre eigenen Fediverse-Instanzen zu betreiben, zu moderieren und zu verwalten.

## Installation

Um die Fedimins-Website lokal auszuführen, führen Sie bitte die folgenden Schritte aus:

1. Stellen Sie sicher, dass Sie eine aktuelle Version von Hugo auf Ihrem System installiert haben.

2. Klonen Sie das Repository mit Git:
```
git clone https://codeberg.org/Fedimins/website.git
```

3. Wechseln Sie in das Verzeichnis der Website:
```
cd website
```

4. Führen Sie den Befehl `hugo server` aus, um den Entwicklungsserver zu starten.

## Verwendung

Nachdem Sie den Entwicklungsserver gestartet haben, können Sie die Fedimins-Website in Ihrem Webbrowser unter der Adresse http://localhost:1313 anzeigen. Der Entwicklungsserver überwacht Änderungen an den Dateien und lädt die Website automatisch neu, wenn Sie Änderungen vornehmen.

## Beitragende

Beiträge zur Fedimins-Website sind willkommen! Wenn Sie Verbesserungen vornehmen möchten, führen Sie bitte die folgenden Schritte aus:

1. Forken Sie das Repository.
2. Erstellen Sie einen neuen Branch für Ihre Änderungen.
3. Führen Sie Ihre Änderungen durch und veröffentlichen Sie diese in Ihrem Fork.
4. Erstellen Sie einen Pull-Request, um Ihre Änderungen in das Hauptrepository einzubringen.

Wir schätzen Ihre Beiträge und werden sie überprüfen und zusammenführen, um die Fedimins-Website kontinuierlich zu verbessern.

## Kontakt

Für weitere Informationen oder bei Fragen besuchen Sie bitte die offizielle [Fedimins-Website](https://fedimins.net/).

## Lizenz

Die Fedimins-Website ist unter der [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](LICENSE) lizenziert. Weitere Informationen finden Sie in der Lizenzdatei.
