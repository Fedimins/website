---
title: About us
date: 2023-01-03T20:34:36.230Z
lastmod: 2023-01-31T07:25:32.851Z
tags:
  - ""
categories:
  - ""
slug: about
---

Fedimins is a collective of administrators and moderators which has several goals:

- Exchange about the work as an administrator/moderator.
- To establish a basic set of rules

With this we want to establish Fedimins as a kind of label, which gives confidence to both users and administrators/moderators. For this purpose we are creating a manifesto, which provides a set of basic rules for instance owners.

Maybe some of you know https://www.chatons.org, personally I (Tealk) think it's an excellent idea and that #Fedimins could also go in this direction.
