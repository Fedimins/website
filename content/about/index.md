---
title: Über uns
date: 2023-01-03T20:34:36.230Z
lastmod: 2023-06-03T12:20:12.917Z
tags:
  - ""
categories:
  - ""
slug: about
---

Fedimins ist ein Kollektiv aus Administrator\*innen und Moderator\*innen im Fediverse. Unser Ziel ist es, ein positives und respektvolles Miteinander im Netzwerk zu fördern. Dabei verfolgen wir mehrere Ziele:

- **Austausch über die Arbeit als Administrator\*in/Moderator\*in:** Bei Fedimins bieten wir Raum für den offenen Dialog und den Erfahrungsaustausch zwischen Administrator\*innen und Moderator\*innen. Wir unterstützen uns gegenseitig, teilen bewährte Praktiken und lernen voneinander.

- **Einen Grundstock an Regeln festlegen:** Wir arbeiten gemeinsam daran, einen Rahmen von grundlegenden Regeln und Richtlinien zu schaffen, an denen sich Instanzbetreiber\*innen orientieren können. Diese Regeln dienen dazu, ein sicheres, inklusives und respektvolles Umfeld für alle Nutzer\*innen zu gewährleisten.

Wir möchten Fedimins als eine Art Label etablieren, das sowohl den Nutzer\*innen als auch den Administrator\*innen/Moderator\*innen Vertrauen vermittelt. Durch die Teilnahme an Fedimins zeigen Administrator\*innen und Moderator\*innen ihr Engagement für eine verantwortungsvolle und professionelle Betreuung ihrer Instanzen.

Bei Fedimins handelt es sich um eine offene Gemeinschaft, eine Inspiration für dieses Projekt ist [Chatons](https://www.chatons.org), bei der jede\*r freiwillig teilnehmen kann. Jeder, der sich für die Arbeit als Administrator\*in oder Moderator\*in im Fediverse interessiert, ist herzlich willkommen, sich uns anzuschließen. Gemeinsam können wir dazu beitragen, das Fediverse zu einem Ort zu machen, an dem sich alle Nutzer\*innen willkommen und sicher fühlen.

Wenn du Fragen hast oder dich beteiligen möchtest, zögere nicht, dich mit uns in Verbindung zu setzen. Zusammen können wir das Fediverse zu einem besseren Ort machen!

Willkommen bei Fedimins – wo wir als Kollektiv von Administrator\*innen und Moderator\*innen gemeinsam das Fediverse voranbringen.
