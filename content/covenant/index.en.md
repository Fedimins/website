---
title: Covenant
description: This is the pre-release version of the Fedimins server agreement
date: 2020-09-01T00:00:00.000Z
lastmod: 2023-12-12T09:37:37.896Z
tags:
  - ""
categories:
  1. ""
slug: covenant
---

Fedimins is a collective of alternative, transparent, open, neutral and solidarity-based hosting providers[^1]. Each member of this collective, hereinafter referred to as "Fedimin", undertakes to respect this charter. Whatever is not explicitly prohibited by this charter or by law is permitted. The Fedimin can have any legal form: a legal entity or a natural person (a private individual, a company, an association, a SCOP, etc.).

It is not necessary to be a Fedimin to participate in the communication, as membership is voluntary and only mandatory for certain services. This is then explicitly mentioned.

**Requirements**

- A Fedimin undertakes to act with respect and goodwill towards the other members of the collective.
- A Fedimin undertakes to act with respect and goodwill towards the users\*inside[^2] of their services.

## Basic obligations

### Hosting

1. A Fedimin undertakes to use only free distributions (GNU/Linux, FreeBSD, etc.) as the operating system for the infrastructure of the users\* services.
2. A Fedimin undertakes to keep the software up to date and to patch gaps as quickly as possible.
3. A Fedimin undertakes to ensure that at least two different (natural) persons have administrator access (root access) to the operating system.
4. A Fedimin undertakes to publicly and unambiguously display its level of control over the hardware and software hosting the services and the associated data. In particular, the name of the hosting provider[^1] that physically hosts the servers must be clearly indicated to users.
5. A Fedimin undertakes to implement a backup policy that enables daily backups.
6. A Fedimin undertakes to permanently delete all information (accounts and personal data) about the user at the user's request within the framework of legal and technical obligations.

### Transparency

1. A Fedimin undertakes to make its technical infrastructure (distribution and software used) transparent. For security reasons, certain information (e.g. the version number of the software) cannot be made publicly accessible.
2. A Fedimin undertakes to do its best to ensure the security of its infrastructure.
3. A Fedimin undertakes not to claim ownership rights to content, data and metadata produced by users.
4. A Fedimin undertakes to publish the terms of use and moderation guidelines in a central location in a clear and generally understandable manner.
  The moderation guidelines should publish both precise rules and guidelines on when and how measures are taken, e.g. [roleplay.monster](https://rollenspiel.monster/charta/mastodon/#moderationsma%C3%9Fnahmen)
5. A Fedimin undertakes to notify users at least 3 months before shutting down the services.

### Data protection

1. A Fedimin undertakes to comply with the GDPR. See [Art. 5](https://dsgvo-gesetz.de/art-5-dsgvo/) and [Art. 6](https://dsgvo-gesetz.de/art-6-dsgvo/)
2. A Fedimin undertakes not to commercially exploit the data or metadata of its users.
3. A Fedimin undertakes to comply exclusively with legally binding requests from authorities[^3].

### Moderation

1. A Fedimin undertakes to actively moderate [^4] in accordance with the GG and StGB.
2. A Fedimin undertakes to respect [human rights](https://unric.org/de/allgemeine-erklaerung-menschenrechte/).
3. A Fedimin undertakes to ensure that moderation is carried out by humans and not by automated processes.
4. A Fedimin undertakes to offer the possibility of appeal at any time.
5. A Fedimin undertakes to always choose the mildest means of moderation that protects other users but does not hinder them.

## Single user instances

**Definition:** A single-user instance refers to an instance that is only used by one person or a few users (e.g. an instance for the family).

Since such an instance is only accessible to a small circle, only the following obligations apply:

- Transparency, paragraph 4
- data protection
- Moderation

---

Contains parts from "[Charter of CHATONS](https://www.chatons.org/charte)" by CHATONS. The text is licensed under the [BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).

[^1]: A hosting provider is defined here as an entity that hosts data provided by third parties and provides services through which that data is run or stored.
[^2]: Those referred to below as "users\" are natural or legal persons who have the ability to create or modify data on the infrastructure.
[^3]: Example [mailbox.org](https://mailbox.org/de/post/auskuenfte-zu-bestandsdaten-verkehrsdaten-und-telekommunikationsueberwachungen), [transparency reports](https://mailbox.org/de/unternehmen#transparenzbericht)
[^4]: "Actively moderate" means that messages or posts that you discover will be moderated as quickly as possible; it does not oblige you to manually search the posts of your instance.
