---
title: Serverabkommen
description: Das ist die vorab Version des Fedimins Serverabkommens
date: 2020-09-01T00:00:00.000Z
lastmod: 2024-05-31T00:11:35.316Z
tags:
  - ""
categories:
  - ""
slug: covenant
---

Fedimins ist ein Kollektiv von alternativen, transparenten, offenen, neutralen und solidarischen Hosting-Anbietern[^1]. Jedes Mitglied dieses Kollektivs, im Folgenden "Fedimin" genannt, verpflichtet sich, die vorliegende Charta zu respektieren. Was nicht explizit durch diese Charta oder das Gesetz verboten ist, ist erlaubt. Der Fedimin kann jede beliebige Rechtsform haben: eine juristische Person oder eine natürliche Person (eine Privatperson, ein Unternehmen, ein Verein, eine SCOP usw.).

Es ist nicht erforderlich, ein Fedimin zu sein, um an der Kommunikation teilzunehmen, da die Mitgliedschaft freiwillig ist und nur für bestimmte Dienste verpflichtend ist. Dies wird dann ausdrücklich erwähnt.

**Anforderungen**

- Ein Fedimin verpflichtet sich, mit Respekt und Wohlwollen gegenüber den anderen Mitgliedern des Kollektivs zu handeln.
- Ein Fedimin verpflichtet sich, mit Respekt und Wohlwollen gegenüber den Nutzer\*innen[^2] ihrer Dienste zu handeln.

## Basisverpflichtungen

### Hosting

1. Ein Fedimin verpflichtet sich, ausschließlich freie Distributionen (GNU/Linux, FreeBSD, etc.) als Betriebssystem für die Infrastruktur der Nutzer\*innen Dienste zu verwenden.
2. Ein Fedimin verpflichtet sich, die Software aktuell zu halten und lücken schnellstmöglich zu patchen.
3. Ein Fedimin verpflichtet sich, dass mindestens zwei verschiedenen (natürlichen) Personen der Administratorzugriff (Root-Zugriff) auf das Betriebssystem ermöglicht wird.
4. Ein Fedimin verpflichtet sich, seinen Kontrollgrad über die Hardware und Software, die die Dienste und die damit verbundenen Daten beherbergt, öffentlich und unmissverständlich anzuzeigen. Insbesondere muss der Name des Hosting-Anbieter[^1], der die Server physisch hostet, gegenüber den Nutzer\*innen klar angegeben werden.
5. Ein Fedimin verpflichtet sich, die Backup-Politik zu implementieren, welche tägliche Backups ermöglicht.
6. Ein Fedimin verpflichtet sich, alle Informationen (Konten und persönliche Daten) über den\*r Nutzer\*in auf dessen Wunsch im Rahmen der gesetzlichen und technischen Verpflichtungen endgültig zu löschen.

### Transparenz

1. Ein Fedimin verpflichtet sich, ihre technische Infrastruktur (Distribution und verwendete Software) transparent zu machen. Aus Sicherheitsgründen können bestimmte Informationen (z. B. die Versionsnummer der Software) nicht öffentlich zugänglich gemacht werden.
2. Ein Fedimin verpflichtet sich, sein Bestes zu tun, um die Sicherheit seiner Infrastruktur zu gewährleisten.
3. Ein Fedimin verpflichtet sich, keine Eigentumsrechte an Inhalten, Daten und Metadaten zu beanspruchen, die von den Nutzer\*innen produziert werden.
4. Ein Fedimin verpflichtet sich, die Nutzungsbedingungen und Moderationsrichtlinien an zentraler Stelle klare und allgemein verständliche zu veröffentlichen.
   In den Moderationsrichtlinien sollen sowohl präzise Regeln als auch Richtlinien darüber zu veröffentlichen, wann und wie Maßnahmen ergriffen werden z.B. [rollenspiel.monster](https://rollenspiel.monster/charta/mastodon/#moderationsma%C3%9Fnahmen)
5. Ein Fedimin verpflichtet sich, mindestens 3 Monate vor Abschaltung der Dienste die Nutzer\*innen zu benachrichtigen.

### Datenschutz

1. Ein Fedimin verpflichtet sich, die DSGVO einzuhalten. Siehe [Art. 5](https://dsgvo-gesetz.de/art-5-dsgvo/) und [Art. 6](https://dsgvo-gesetz.de/art-6-dsgvo/)
2. Ein Fedimin verpflichtet sich, die Daten oder Metadaten seiner Nutzer\*innen nicht kommerziell zu Verwerten.
3. Ein Fedimin verpflichtet sich, außschließlich rechtlich zwingende Behördenanfragen[^3] nachzukommen.

### Moderation

1. Ein Fedimin verpflichtet sich, nach GG und StGB aktiv[^4] zu moderieren.
2. Ein Fedimin verpflichtet sich, der Achtung der [Menschenrechte](https://unric.org/de/allgemeine-erklaerung-menschenrechte/).
3. Ein Fedimin verpflichtet sich, dass die Moderation durch Menschen durchgeführt wird und nicht über automatisierte Verfahren.
4. Ein Fedimin verpflichtet sich, jederzeit eine Wiederspruchsmöglichkeit anzubieten.
5. Ein Fedimin verpflichtet sich, bei der Moderation ist immer das mildeste Mittel zu wählen, das andere User schützt, aber nicht behindert.

## Singeluserinstanzen

**Definition:** Eine Single-User-Instanz bezieht sich auf eine Instanz, die nur von einer Person bzw. ein paar User(z.B. eine Instanz für die Familie) genutzt wird.

Da eine solche Instanz nur einem Kleinen kreis zugägnlich ist, gelten nur folgende Verplfichtungen:

- Transparenz Absatz 4
- Datenschutz
- Moderation

---

Beinhaltet Teile aus "[Charter of CHATONS](https://www.chatons.org/charte)" von CHATONS. Der Text ist lizenziert unter der [BY-SA 4.0 Lizenz](https://creativecommons.org/licenses/by-sa/4.0/).

[^1]: Ein Hosting-Anbieter wird hier definiert als eine Einheit, die von Dritten bereitgestellte Daten beherbergt und Dienstleistungen anbietet, über die diese Daten laufen oder gespeichert werden.
[^2]: Die im Folgenden als "Nutzer_innen" bezeichneten sind natürliche oder juristische Personen, die die Möglichkeit haben, Daten auf der Infrastruktur zu erstellen oder zu ändern.
[^3]: Beispiel [mailbox.org](https://mailbox.org/de/post/auskuenfte-zu-bestandsdaten-verkehrsdaten-und-telekommunikationsueberwachungen), [Transparenzberichte](https://mailbox.org/de/unternehmen#transparenzbericht)
[^4]: "Aktiv moderieren" bedeutet, dass Meldungen oder Beiträge die ihr entdeckt, schnellstmöglich moderiert werden; es verpflichtet nicht zur manuellen Durchsuchung der Beiträge eurer Instanz.
