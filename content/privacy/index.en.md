---
title: Privacy policy
date: 2020-09-01T00:00:00.000Z
lastmod: 2023-01-19T17:06:00.986Z
tags:
  - ""
categories:
  - ""
slug: privacy
---

## Introduction

We have made a conscious decision not to use tracking cookies, analysis tools or other direct integration of external content. Operators of linked websites, get your information only when you click on the link. This approach is not the "mainstream" entsprichend and has disadvantages for the ranking in Google and Co. but we theirtwillen gladly accept. We do this to promote the [informational self-determination](https://de.wikipedia.org/wiki/Informationelle_Selbstbestimmung) and not to endanger your IT security unnecessarily.

The terms used are not gender-specific.

## Name and contact details

Daniel, Buck<br/>
<span style="font-size: 10px;">Herzog von Meranien</span><br/>
Kirchenstr. 2<br/>
92693 Eslarn<br/>
**E-mail address:** [webmaster@fedimins.net](mailto:webmaster@fedimins.net)<br/>
**Phone:** +49 156 78563777<br/>
**Imprint:** [https://fedimins.net/imprint/](/imprint)

## Ansprechpartner für datenschutzrechtliche Fragen

If you have any questions about privacy, please contact us at [datenschutz@fedimins.net](mailto:datenschutz@fedimins.net) or at the postal address above.

## Provision of the online offer and web hosting

### Collection of access data and log files

When you visit our website, your browser systematically transmits certain information to the server that could in turn make you identifiable. For example, you or your web browser transmit information such as the IP address assigned by your provider.

In principle, you cannot prevent this system-related behavior of your browser and therefore you have no choice but to trust that we handle the transmitted information sensitively. Because - even if it may sound a bit trite - the protection of your data is actually close to our hearts, we will handle the transmitted information in the best possible way to represent your interests. For this reason, we do not collect, process or use the submitted information for analysis, advertising or similar purposes. Rather, we do not store any information about you - the web server based on nginx is configured so that it does not write log files:

```
## LOGS ##
access_log off;
error_log off;
```

### Use of cookies

Cookies are text files that contain data from visited websites or domains and are stored by a browser on the user's computer. The primary purpose of a cookie is to store information about a user during or after his or her visit within an online offering. Stored information may include, for example, language settings on a website, login status, a shopping cart, or where a video was watched. The term cookies also includes other technologies that perform the same functions as cookies (e.g., when user information is stored using pseudonymous online identifiers, also known as "user IDs").

The data processed by the cookies are necessary for the provision of our services and thus to protect our legitimate interests as well as those of third parties pursuant to Art. 6 (1) p. 1 lit. f DSGVO. The cookies set, which are necessary for logging in, for example, are persistent cookies, although they should not be confused with third-party tracking cookies that track a user across pages on the Internet.

**Note**: Most browsers accept cookies automatically. However, you can configure your browser so that no cookies are stored on your computer or a notice always appears before a new cookie is created. However, the complete deactivation of cookies may mean that you cannot use all the functions of our website.

## Contacting us

When contacting us (e.g. via contact form, email, telephone or via social media), the information of the inquiring persons will be processed as far as this is necessary to answer the contact requests and any requested measures. In this context, the provision of a valid e-mail address is required so that I know from whom the request originates and in order to be able to answer it. Further information can be provided voluntarily.

## Presence in social networks (social media)

We maintain online presences within social networks and process user data in this context in order to communicate with users active there or to offer information about us.

We would like to point out that user data may be processed outside the European Union. This may result in risks for users because, for example, it could make it more difficult to enforce the rights of users.

## Deletion of data

The data processed by us will be deleted in accordance with the legal requirements as soon as their consents permitted for processing are revoked or other permissions cease to apply (e.g. if the purpose of processing this data has ceased to apply or it is not required for the purpose).

If the data are not deleted because they are required for other and legally permissible purposes, their processing will be limited to these purposes. That is, the data is blocked and not processed for other purposes. This applies, for example, to data that must be retained for reasons of commercial or tax law or whose storage is necessary for the assertion, exercise or defense of legal claims or for the protection of the rights of another natural or legal person.

Further information on the deletion of personal data can also be found in the individual data protection notices.

## Change and update of the data protection notice

Due to the further development of my website and offers on it or due to changed legal or regulatory requirements, it may become necessary to change this privacy policy. The current data protection notice can be accessed and printed out by you at any time on the website under the link.

## Rights of the data subjects

As a data subject, you are entitled to various rights under the GDPR:

- **Pursuant to Art.15 DSGVO** to request information about your personal data processed by me. In particular, you can request information about the processing purposes, the category of personal data, the categories of recipients to whom your data has been or will be disclosed, the planned storage period, the existence of a right to rectification, erasure, restriction of processing or objection, the existence of a right of complaint, the origin of your data, if it has not been collected by me, as well as the existence of automated decision-making, including profiling, and, if applicable, meaningful information about its details.

- Pursuant to Art.16 DSGVO\*\*, to demand the correction or completion of your personal data stored by me without delay.

- Pursuant to Art.17 DSGVO\*\*, to request the deletion of your personal data stored by me, unless the processing is necessary for the exercise of the right to freedom of expression and information, for compliance with a legal obligation, for reasons of public interest or for the assertion, exercise or defense of legal claims.

- **Pursuant to Art.18 DSGVO**, to request the restriction of the processing of your personal data, insofar as the accuracy of the data is disputed by you, the processing is unlawful, but you object to its erasure, I no longer require the data, but you need them for the assertion, exercise or defense of legal claims or you have objected to the processing pursuant to Art.21 DSGVO

- Pursuant to Art.20 DSGVO\*\*, to receive your personal data that you have provided to me in a structured, common and machine-readable format or to request that it be transferred to another responsible party

- **Pursuant to Art.7 para.3 DSGVO** to revoke your consent once given to me at any time. This has the consequence that I may not continue the data processing, which was based on this consent, for the future.

- **Pursuant to Art.77 DSGVO** to complain to a supervisory authority. As a rule, you can contact the supervisory authority of your usual place of residence or workplace for this purpose

## External payment service providers

Visitors to this website can support the operator financially via payment service providers. The privacy policies of the respective providers apply:

- Open Collective: [Open Collective Privacy Policy](https://opencollective.com/privacypolicy)

## Address processing

All the contact information of me given on this website, including any photos, are expressly for information purposes only or for contacting me. In particular, they may not be used for sending advertising, spam and the like. An advertising use of these data is therefore hereby contradicted. Should this information nevertheless be used for the aforementioned purposes, I reserve the right to take legal action.

## Licenses

- Fediverse Logo, Eukombos, CC 0 1.0, [File:Fediverse logo proposal.svg - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Fediverse_logo_proposal.svg)
- Gear, Font Awesome, CC BY 4.0, [Gear Classic Solid Icon | Font Awesome](https://fontawesome.com/icons/gear?s=solid&f=classic)
